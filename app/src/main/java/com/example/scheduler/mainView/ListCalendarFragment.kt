package com.example.scheduler.mainView

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.scheduler.databinding.ListCalendarFragmentBinding

class ListCalendarFragment : Fragment() {
    private lateinit var binding: ListCalendarFragmentBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = ListCalendarFragmentBinding.inflate(LayoutInflater.from(context))
        return binding.root
    }
}