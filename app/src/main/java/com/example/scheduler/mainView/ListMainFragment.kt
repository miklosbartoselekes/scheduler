package com.example.scheduler.mainView

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.scheduler.adapter.ScheduleAdapter
import com.example.scheduler.data.SchedulerDatabase
import com.example.scheduler.data.TaskItem
import com.example.scheduler.databinding.ListMainFragmentBinding
import com.example.scheduler.detailedView.DetailsActivity
import kotlin.concurrent.fixedRateTimer
import kotlin.concurrent.thread

class ListMainFragment: Fragment(), ScheduleAdapter.TaskItemClickListener,
    NewTaskItemDialogFragment.NewTaskItemDialogListener {

    private lateinit var binding: ListMainFragmentBinding

    private lateinit var database: SchedulerDatabase
    private lateinit var adapter: ScheduleAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        binding = ListMainFragmentBinding.inflate(LayoutInflater.from(context))

        database = SchedulerDatabase.getDatabase(requireActivity().applicationContext)

        binding.fab.setOnClickListener {
            NewTaskItemDialogFragment().show(
                childFragmentManager,
                NewTaskItemDialogFragment.TAG
            )
        }

        initRecyclerView()

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        loadItemsInBackground()
    }

    private fun initRecyclerView() {
        adapter = ScheduleAdapter(this)
        binding.rvMain.layoutManager = LinearLayoutManager(activity)
        binding.rvMain.adapter = adapter
        loadItemsInBackground()
    }

    private fun loadItemsInBackground() {
        thread {
            val items = database.taskItemDao().getAll()
            requireActivity().runOnUiThread {
                adapter.update(items)
            }
        }
    }

    override fun onItemChanged(item: TaskItem) {
        thread {
            database.taskItemDao().update(item)
            Log.d("ListActivity","TaskItem update was succesfull")
            val items = database.taskItemDao().getAll()
            requireActivity().runOnUiThread {
                adapter.update(items)
            }
        }
    }

    override fun onTaskItemDeleted(item: TaskItem) {
        thread {
            database.taskItemDao().deleteItem(item)
            requireActivity().runOnUiThread {
                adapter.delete(item)
            }
        }
    }

    override fun onTaskItemSelected(item: TaskItem) {
        val intent = Intent(activity, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.KEY_TASK_ID,item.id)
        startActivity(intent)
    }

    override fun onTaskItemCreated(newItem: TaskItem) {
        thread {
            val insertID = database.taskItemDao().insert(newItem)
            newItem.id = insertID
            requireActivity().runOnUiThread {
                adapter.addItem(newItem)
            }
        }
    }
}