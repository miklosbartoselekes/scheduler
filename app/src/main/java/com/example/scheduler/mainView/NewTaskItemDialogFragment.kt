package com.example.scheduler.mainView

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.scheduler.R
import com.example.scheduler.data.TaskItem
import com.example.scheduler.databinding.DialogNewTaskItemBinding
import java.util.*

class NewTaskItemDialogFragment : DialogFragment() {
    interface NewTaskItemDialogListener {
        fun onTaskItemCreated(newItem: TaskItem)
    }

    private lateinit var listener: NewTaskItemDialogListener

    private lateinit var binding: DialogNewTaskItemBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = this.parentFragment as? NewTaskItemDialogListener
            ?: throw RuntimeException("Activity must implement the NewShoppingItemDialogListener interface!")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DialogNewTaskItemBinding.inflate(LayoutInflater.from(context))

        return AlertDialog.Builder(requireContext())
            .setTitle(R.string.new_task_item)
            .setView(binding.root)
            .setPositiveButton(R.string.button_ok) { dialogInterface, i ->
                if (isValid()) {
                    listener.onTaskItemCreated(getTaskItem())
                }
            }
            .setNegativeButton(R.string.button_cancel, null)
            .create()
    }

    private fun isValid(): Boolean {
        val startdate = Date(binding.dpStartDate.year,binding.dpStartDate.month,binding.dpStartDate.dayOfMonth)
        val finishdate = Date(binding.dpFinishDate.year,binding.dpFinishDate.month,binding.dpFinishDate.dayOfMonth)
        if (startdate.after(finishdate) == true)
            return false
        else
            return binding.etName.text.isNotEmpty() && binding.etTimeToSolve.text.isNotEmpty()
    }


    private fun getTaskItem() = TaskItem(
        name = binding.etName.text.toString(),
        isFinished = binding.cbFinished.isChecked,
        startDate = Date(binding.dpStartDate.year,binding.dpStartDate.month,binding.dpStartDate.dayOfMonth),
        finishDate = Date(binding.dpFinishDate.year,binding.dpFinishDate.month,binding.dpFinishDate.dayOfMonth),
        hoursToSolve = binding.etTimeToSolve.text.toString().toDouble(),
        hoursSpent = 0.0
    )

    companion object {
        const val TAG = "NewTaskItemDialogFragment"
    }
}