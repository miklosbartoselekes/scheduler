package com.example.scheduler.mainView

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class ListPagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {
    companion object {
        private const val NUM_PAGES: Int = 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ListMainFragment()
            1 -> ListCalendarFragment()
            else -> ListMainFragment()
        }
    }

    override fun getItemCount(): Int = NUM_PAGES
}