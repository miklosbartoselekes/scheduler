package com.example.scheduler.mainView

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TableLayout
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.scheduler.detailedView.DetailsActivity
import com.example.scheduler.R
import com.example.scheduler.adapter.ScheduleAdapter
import com.example.scheduler.data.SchedulerDatabase
import com.example.scheduler.data.TaskItem
import com.example.scheduler.databinding.ActivityListBinding
import com.example.scheduler.mainView.NewTaskItemDialogFragment.*
import com.google.android.material.tabs.TabLayoutMediator
import kotlin.concurrent.thread

class ListActivity : AppCompatActivity(){
    private lateinit var binding: ActivityListBinding
    private var nightMode:Boolean = (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
    }

    override fun onResume() {
        super.onResume()
        val listPagerAdapter = ListPagerAdapter(this)
        binding.mainViewPager.adapter = listPagerAdapter



        TabLayoutMediator(binding.tabLayout, binding.mainViewPager) { tab, position ->
            tab.text = when(position) {
                0 -> getString(R.string.List)
                1 -> getString(R.string.calendar)
                else -> ""
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.custom -> {
                nightMode = !nightMode
                if(nightMode) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                }
                else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}