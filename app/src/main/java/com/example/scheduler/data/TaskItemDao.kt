package com.example.scheduler.data

import androidx.room.*

@Dao
interface TaskItemDao {
    @Query("SELECT * FROM taskitem ORDER BY id DESC")
    fun getAll(): List<TaskItem>

    @Query("SELECT * FROM taskitem WHERE id=:taskID")
    fun getItem(taskID: Long): TaskItem

    @Insert
    fun insert(taskItems: TaskItem): Long

    @Update
    fun update(taskItem: TaskItem)

    @Delete
    fun deleteItem(taskItem: TaskItem)
}