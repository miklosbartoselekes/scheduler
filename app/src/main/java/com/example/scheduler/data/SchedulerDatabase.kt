package com.example.scheduler.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import java.util.*

@Database(entities = [TaskItem::class], version = 1)
@TypeConverters(value = [TaskItem.Converter::class])
abstract class SchedulerDatabase : RoomDatabase() {
    abstract fun taskItemDao(): TaskItemDao

    companion object {
        fun getDatabase(applicationContext: Context): SchedulerDatabase {
            return Room.databaseBuilder(
                applicationContext,
                SchedulerDatabase::class.java,
                "task-list"
            ).build();
        }
    }
}