package com.example.scheduler.data
import androidx.room.*
import java.util.*

@Entity(tableName = "taskitem")
data class TaskItem(
    @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "startdate") var startDate: Date,
    @ColumnInfo(name = "finishdate") var finishDate: Date,
    @ColumnInfo(name = "hourstosolve") var hoursToSolve: Double,
    @ColumnInfo(name = "hoursspent") var hoursSpent: Double,
    @ColumnInfo(name = "is_Finished") var isFinished: Boolean
) {
    class Converter {
        @TypeConverter
        fun fromLongToDate(value: Long?): java.util.Date? {
            return value?.let { Date(it) }
        }

        @TypeConverter
        fun fromDateToLong(date: java.util.Date?): Long? {
            return date?.time?.toLong()
        }
    }
}
