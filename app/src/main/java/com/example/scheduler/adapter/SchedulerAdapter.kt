package com.example.scheduler.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.scheduler.data.TaskItem
import com.example.scheduler.databinding.ItemScheduleListBinding

class ScheduleAdapter(private val listener: TaskItemClickListener) :
    RecyclerView.Adapter<ScheduleAdapter.TaskViewHolder>() {

    private val items = mutableListOf<TaskItem>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TaskViewHolder(
        ItemScheduleListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        val taskItem = items[position]

        holder.binding.cbIsBought.isChecked = taskItem.isFinished;
        holder.binding.tvName.text = taskItem.name;
        holder.binding.tvDate.text = "${taskItem.startDate.year}.${taskItem.startDate.month + 1}.${taskItem.startDate.date} - ${taskItem.finishDate.year}.${taskItem.finishDate.month + 1}.${taskItem.finishDate.date}";


        if(taskItem.isFinished) {
            holder.binding.tvProcess.text = "Finished"
        }
        else {
            val proc = ((taskItem.hoursSpent.toDouble()/taskItem.hoursToSolve.toDouble()) * 100).toInt()
            holder.binding.tvProcess.text = "${taskItem.hoursToSolve - taskItem.hoursSpent} hours left - ${proc.toString()}%"
        }

        holder.binding.cbIsBought.setOnCheckedChangeListener { buttonView, isChecked ->
            taskItem.isFinished = isChecked
            listener.onItemChanged(taskItem)
        }

        holder.binding.ibDelete.setOnClickListener {
            listener.onTaskItemDeleted(taskItem)
        }

        holder.binding.ibSet.setOnClickListener {
            listener.onTaskItemSelected(taskItem)
        }
    }

    fun addItem(item: TaskItem) {
        items.add(0,item)
        notifyItemInserted(0)
    }

    fun update(taskItems: List<TaskItem>) {
        items.clear()
        items.addAll(taskItems)
        notifyDataSetChanged()
    }

    fun delete(item: TaskItem) {
        items.remove(item)
        notifyDataSetChanged()
    }

    override  fun getItemCount(): Int = items.size

    interface TaskItemClickListener {
        fun onItemChanged(item: TaskItem)
        fun onTaskItemDeleted(item: TaskItem)
        fun onTaskItemSelected(item: TaskItem)
    }

    inner class TaskViewHolder(val binding: ItemScheduleListBinding) : RecyclerView.ViewHolder(binding.root)
}