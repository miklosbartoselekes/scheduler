package com.example.scheduler.detailedView

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import com.example.scheduler.data.SchedulerDatabase
import com.example.scheduler.data.TaskItem
import com.example.scheduler.databinding.ActivityDetailsBinding
import java.util.*
import kotlin.concurrent.thread

class DetailsActivity : AppCompatActivity(),
StopWatchFragment.StopwatchDialogListener{

    private lateinit var binding: ActivityDetailsBinding

    private lateinit var database: SchedulerDatabase


    private var task: TaskItem? = null

    companion object {
        const val KEY_TASK_ID = "KEY_TASK_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.title = "Details"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        database = SchedulerDatabase.getDatabase(applicationContext)

        val taskID = this.intent.getLongExtra(KEY_TASK_ID, -1)

        setBindingListener()

        thread {
            task = database.taskItemDao().getItem(taskID)
            setDetails()
        }
    }

    override fun onPause() {
        super.onPause()
        saveData()
        thread {task?.let { database.taskItemDao().update(it) }}
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun saveData() {
        if(binding.etName.text.toString().isNotEmpty())
            task?.name = binding.etName.text.toString()
        safecheckFinishDate(binding.dpFinishDate.year,binding.dpFinishDate.month,binding.dpFinishDate.dayOfMonth)
        task?.startDate = Date(binding.dpStartDate.year,binding.dpStartDate.month,binding.dpStartDate.dayOfMonth)
        task?.finishDate = Date(binding.dpFinishDate.year,binding.dpFinishDate.month,binding.dpFinishDate.dayOfMonth)
        if(binding.etTimeToSolve.text.toString().isNotEmpty())
            task?.hoursToSolve = binding.etTimeToSolve.text.toString().toDouble()
        safecheckTimeSpent()
        if(binding.etTimeSpent.text.toString().isNotEmpty())
            task?.hoursSpent = binding.etTimeSpent.text.toString().toDouble()
        task?.isFinished = binding.cbFinished.isChecked
    }

    private fun setBindingListener() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            binding.dpStartDate.setOnDateChangedListener { datePicker, y, m, d ->
                safecheckStartDate(y,m,d)
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            binding.dpFinishDate.setOnDateChangedListener { datePicker, y, m, d ->
                safecheckFinishDate(y, m, d)
            }
        }

        binding.bStopWatch.setOnClickListener {
            StopWatchFragment().show(
                supportFragmentManager,
                StopWatchFragment.TAG
            )
        }

        binding.etTimeSpent.setOnFocusChangeListener { view, b ->
            if(!b) {
                safecheckTimeSpent()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    binding.pbProgress.setProgress(
                        (binding.etTimeSpent.text.toString().toDouble()/binding.etTimeToSolve.text.toString().toDouble()*100.0).toInt(),true
                    )}
            }
        }

        binding.etTimeToSolve.setOnFocusChangeListener { view, b ->
            if(!b) {
                safecheckTimeSpent()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    binding.pbProgress.setProgress(
                        (binding.etTimeSpent.text.toString().toDouble()/binding.etTimeToSolve.text.toString().toDouble()*100.0).toInt(),true)
                }
            }
        }

        binding.bSet.setOnClickListener {
            finish()
        }
    }

    private fun safecheckStartDate(y: Int,m: Int,d: Int) {
        val startdate = Date(y,m,d)
        val finishdate = Date(binding.dpFinishDate.year, binding.dpFinishDate.month,binding.dpFinishDate.dayOfMonth)
        if(finishdate.before(startdate) == true)
            binding.dpFinishDate.updateDate(startdate.year, startdate.month, startdate.date)
    }

    private fun safecheckFinishDate(y: Int,m: Int,d: Int) {
        val finishdate = Date(y,m,d)
        val startdate = Date(binding.dpStartDate.year, binding.dpStartDate.month,binding.dpStartDate.dayOfMonth)
        if(startdate.after(finishdate) == true)
            binding.dpStartDate.updateDate(finishdate.year, finishdate.month, finishdate.date)
    }

    private fun safecheckTimeSpent() {
        if(binding.etTimeSpent.text.toString().toDouble() > binding.etTimeToSolve.text.toString().toDouble())
            binding.etTimeSpent.text = binding.etTimeToSolve.text
    }

    private fun setDetails() {
        binding.etName.setText(task?.name,TextView.BufferType.NORMAL)
        binding.dpStartDate.updateDate(task!!.startDate.year,task!!.startDate.month,task!!.startDate.date)
        binding.dpFinishDate.updateDate(task!!.finishDate.year,task!!.finishDate.month,task!!.finishDate.date)
        binding.etTimeToSolve.setText(task?.hoursToSolve.toString(),TextView.BufferType.NORMAL)
        binding.etTimeSpent.setText(task?.hoursSpent.toString(),TextView.BufferType.NORMAL)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.pbProgress.setProgress(
                (binding.etTimeSpent.text.toString().toDouble()/binding.etTimeToSolve.text.toString().toDouble()*100.0).toInt(),true)
        }
    }

    override fun onStopWatch(measuredTime: Int) {
        val int = measuredTime/36
        val timeInHours = int/100.0
        binding.etTimeSpent.setText("${binding.etTimeSpent.text.toString().toDouble() + timeInHours}")
        safecheckTimeSpent()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.pbProgress.setProgress(
                (binding.etTimeSpent.text.toString().toDouble()/binding.etTimeToSolve.text.toString().toDouble()*100.0).toInt(),true)
        }
    }
}