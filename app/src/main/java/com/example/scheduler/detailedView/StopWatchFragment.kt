package com.example.scheduler.detailedView

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.scheduler.R
import com.example.scheduler.databinding.DialogStopwatchBinding
import java.util.*

class StopWatchFragment : DialogFragment() {
    interface StopwatchDialogListener {
        fun onStopWatch(measuredTime: Int)
    }

    private lateinit var listener: StopwatchDialogListener
    private var measuredTime: Int = 0
    private var isRunning: Boolean = false

    private var handler: Handler = Handler()
    private lateinit var runnable: Runnable

    private lateinit var binding: DialogStopwatchBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? StopwatchDialogListener
            ?: throw RuntimeException("Activity must implement the StopwatchDialogListener interface!")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DialogStopwatchBinding.inflate(LayoutInflater.from(context))

        runnable = Runnable {
            measuredTime++;
            setText()
            handler.postDelayed(runnable,1)
        }

        binding.bStartStop.setOnClickListener {
            isRunning = !isRunning
            if(isRunning) {
                binding.bStartStop.setText(R.string.stop)
                handler.postDelayed(runnable, 1)
            }
            else {
                binding.bStartStop.setText(R.string.start)
                handler.removeCallbacks(runnable)
            }
        }

        binding.bReset.setOnClickListener { measuredTime = 0; setText() }

        return AlertDialog.Builder(requireContext())
            .setTitle("Stopwatch")
            .setView(binding.root)
            .setPositiveButton(getString(R.string.Finished)) { dialogInterface, i ->
                listener.onStopWatch(measuredTime)
            }
            .setNegativeButton(R.string.button_cancel, null)
            .create()
    }

    private fun setText() {
        var hour = "${measuredTime/3600}"
        if(hour.length == 1) hour = "0" + hour
        var minute = "${measuredTime/60%60}"
        if(minute.length == 1) minute = "0" + minute
        var sec = "${measuredTime%60}"
        if(sec.length == 1) sec = "0" + sec
        binding.tvmeasuredTime.setText("${hour}:${minute}:${sec}")
    }

    companion object {
        const val TAG = "StopWatchDialogFragment"
    }
}